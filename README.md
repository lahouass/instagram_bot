# Instagram_Bot

For creating an Instagram_Bot that will follow or unfollow influencers automatically

######################################################

This project is for creating an instagram bot that will
follow or unfollow people on instagram automatically.

######################################################

# By Loris Davy Nounagnon Ahouassou
# on 7/02/2020

>> Developped in Python 3.6
>> Modules used : selenium and time

For those using the firefox browser, you must add "geckodriver.log" at the root of your project.

Note: I already installed it for you.

SO HOW TO USE THIS PROJECT ?

1 - Firstly, you must put your password in "dont_see_mypassword.py" file.

2 - Secondly, for the test, you must specify your username and the influencer's username you're looking for in the "Instagram_Bot.py" file.

3 - Execute Instagram_Bot.py and enjoy !