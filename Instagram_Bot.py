########## Create a mini-bot to follow or un follow an influencer on instagram ####################

# Author : Loris Davy Nounagnon Ahouassou
# Date : 7/02/2020


from selenium import webdriver
from time import sleep
from dont_see_mypassword import pwd

class Instagrambot:

    def __init__(self, username, pwd):
        self.username = username
        self.pwd = pwd
        self.driver = webdriver.Firefox()
        self.driver.get("https://instagram.com")
        sleep(4)
        #Go to Log in page
        self.driver.find_element_by_xpath("//a[contains(text(), 'Log in')]").click()
        #Put username and password
        self.driver.find_element_by_xpath("//input[@name=\"username\"]").send_keys(username)
        self.driver.find_element_by_xpath("//input[@name=\"password\"]").send_keys(pwd)
        #submit
        self.driver.find_element_by_xpath('//button[@type="submit"]').click()
        sleep(4)
        # For set Notifications on "Not now" 
        self.driver.find_element_by_xpath("/html/body/div[4]/div/div/div[3]/button[2]").click()
        sleep(2)

    #To find an influencer
    def search(self, Influenceur):
        self.driver.find_element_by_xpath("/html/body/div[1]/section/nav/div[2]/div/div/div[2]/input")\
            .send_keys(Influenceur)
        sleep(2)
        self.driver.find_element_by_xpath("/html/body/div[1]/section/nav/div[2]/div/div/div[2]/div[2]/div[2]/div/a[1]/div/div[2]/div/span")\
            .click()
        sleep(2)

    #unfollow or follow the influencer (just by a click)
    def unfollow_or_follow(self):
        self.driver.find_element_by_xpath("/html/body/div[1]/section/main/div/header/section/div[1]/div[1]/span/span[1]/button")\
            .click()
        sleep(2)
        self.driver.find_element_by_xpath("/html/body/div[4]/div/div/div[3]/button[1]").click()

# Testing
MyBot = Instagrambot("put_your_username", pwd())
MyBot.search("put_the_influencer_username")

#if you already have followed the influencer, then "unfollow", else "follow" 
MyBot.unfollow_or_follow()


